# DuctApe

## Description

DuctApe is a software suite that will help bioinformaticians to analyze genomes AND phenomic experiments.
The final purpose of the program is to combine the genomic informations (encoded as KEGG pathways) with the results of phenomic experiments (Phenotype Microarrays) and highlight the genes that may be responsible for phenotypic variations.
DuctApe it's written in python and works as a command line tool for Unix environments.