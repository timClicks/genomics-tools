# dDocent

## Description

From the github repo:
This script serves as an interactive bash wrapper to QC, assemble, map, and call SNPs from almost any kind of RAD data. It is designed to run on Linux based machines with large memory capacity and multiple processing cores.

