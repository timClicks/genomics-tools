# BayeScan

## Description from the user manual

BayeScan v2.1
User Manual
Matthieu Foll
January, 2012
1.
Introduction
This program,BayeScan aims at identifying candida te loci under natural selection from genetic data, using differences in allele frequencies between populations. BayeScan is based on the multinomial - Dirichlet model.  One  of  the  scenarios  covered  consists  of  an  island  model  in  which  subpopulation allele  frequencies  are  correlated  through a  common  migrant  gene  pool  from  which  they  differ  in varying  degrees.  The  difference  in  allele  frequency  between  this  common  gene  pool  and  each subpopulation  is  measured  by  a  subpopulation  specific  F ST coefficient.  Therefore,  this  formulation can  consider realistic  ecological  scenarios  where  the  effective  size  and  the  immigration  rate  may differ among subpopulations.
