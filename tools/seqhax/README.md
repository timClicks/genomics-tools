# Seqhax

## Description from docs

A seqtk-style toolkit for sequence analysis. By no means feature complete. In fact largely contains features other authors have not merged into their respective tools.

It can be used to do the following things:

    anon       -- Rename sequences by a sequential number
    convert    -- Convert between FASTA and FASTQ formats
    filter     -- Filter reads from a sequence file
    pairs      -- (De)interleave paired end reads
    pecheck    -- Check that paired end reads match properly (also join them)
    preapp     -- Prepend or append string to sequences
    randseq    -- Generate a random sequence file
    stats      -- Basic statistics about sequence files
    trunc      -- Truncate sequences
