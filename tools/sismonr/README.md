# sismonr

## Description

The R package sismonr is a tool for the simulation of gene expression profiles for in silico regulatory networks. The package generates gene regulatory networks, which include protein-coding and noncoding genes linked via different types of regulation: regulation of transcription, translation, RNA or protein decay, and post-translational modifications. The effect of genetic mutations on the system behaviour is accounted for via the simulation of genetically different in silico individuals. The ploidy of the system is not restricted to the usual haploid or diploid situations, but is defined by the user. A choice of stochastic simulation algorithms allow us to simulate the expression profiles (RNA and if applicable protein abundance) of the genes in the in silico system for the different in silico individuals. A tutorial explaining how to use the package is available at <https://oliviaab.github.io/sismonr/>.

